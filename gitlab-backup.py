import requests
import tabulate
import os
import git
from tldextract import extract
from configuration import *


# Returns id and username
def retrieve_userid(credential):
    response = requests.get(credential['API_ENDPOINT'] + "/user", params={'private_token': credential['PRIVATE_API_TOKEN']})
    assert response.status_code == 200
    response_json = response.json()
    return response_json['id'], response_json['username']


def retrieve_projects_user_is_member_of(credential):
    response = requests.get(credential['API_ENDPOINT'] + "/projects", params={'private_token': credential['PRIVATE_API_TOKEN'],
                                                                'membership': True,
                                                                'pagination': 'keyset',
                                                                'per_page': 20,
                                                                "order_by": 'id',
                                                                'sort': 'desc'})
    print(response)
    assert response.status_code == 200
    r = response.json()

    next = None
    if response.links.get('next') is not None:
        next = response.links['next']['url']

    while next is not None:
        response = requests.get(next)
        assert response.status_code == 200
        r = r + response.json()

        next = None
        if response.links.get('next') is not None:
            next = response.links['next']['url']

        print(".", end="")

    projects = []
    for project in r:
        projects.append({
            "name": project['name'],
            "path": project['path'],
            "path_with_namespace": project['path_with_namespace'],
            "ssh": project['ssh_url_to_repo'],
            "http": project['http_url_to_repo'],
        })

    return projects


def is_already_cloned(path):
    return os.path.isdir(path + "/.git")


if __name__ == '__main__':
    # print("Retrieving user details")
    # userid, username = retrieve_userid()

    for credential in CREDENTIALS:

        tsd, td, tsu = extract(credential['API_ENDPOINT'])
        instance_domain = ("" if tsd == "" else f"{tsd}.") + f"{td}.{tsu}"

        print(f"Retrieving user projects for {instance_domain}", end="")
        projects = retrieve_projects_user_is_member_of(credential)
        print("")
        print(tabulate.tabulate(projects, tablefmt="pretty", stralign="left"))

        for i, project in enumerate(projects):
            print("Progress: " + str(i + 1) + " of " + str(len(projects)))
            print("Current: " + project['path_with_namespace'])

            path = ARCHIVE_DIR + "/" + instance_domain + "/" + project['path_with_namespace']
            os.makedirs(path, exist_ok=True)

            try:

                if is_already_cloned(path):
                    # git pull origin master
                    print("  pulling latest changes from origin master")
                    repository = git.Repo(path)
                    origin = repository.remotes.origin
                    origin.pull()
                else:
                    # git clone
                    print("  cloning repository")
                    git.Repo.clone_from(project['ssh'], path)
            except:
                print("Error while cloning/pulling")

        print("Completed!")
