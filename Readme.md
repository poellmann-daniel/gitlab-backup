# Create Backup of Gitlab Repositories

*Clones* and *updates* all projects a user is a member of via ssh. 

# Instructions
## 1. API Key
Needs an API key for your gitlab user account with the ```read_api``` permission.

## 2. Configuration
Copy ```configuration_template.py``` to ```configuration.py``` and change the parameters accordingly:

    ARCHIVE_DIR = "archive/"

    CREDENTIALS = [
        {
            "PRIVATE_API_TOKEN": "--your--private--api--token--",
            "API_ENDPOINT": "https://gitlab.com/api/v4",
        },
        {
            "PRIVATE_API_TOKEN": "--your--private--api--token--",
            "API_ENDPOINT": "https://gitlab.inf.ethz.ch/api/v4",
        }
    ]

where ```ARCHIVE_DIR``` is the path to the folder where you would like to store the repositories. 

## 3. Python Dependencies
    pip install -r requirements.txt

Also, ```git``` needs to be installed in your ```PATH```.

## 4. Backup
Run ```python gitlab-backup.py```

